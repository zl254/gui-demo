package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;

//here main means client
public class Main extends Application {

    private Stage window;

    private void showMain() throws IOException {
        FXMLLoader mainRoot = new FXMLLoader(getClass().getResource("Main.fxml"));
        mainRoot.setControllerFactory(c->{
            try {
                return new mainController(window);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });
        Scene mainScene=new Scene(mainRoot.load());
        window.setTitle("RISC Game");
        window.setScene(mainScene);
        window.show();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.window=primaryStage;
        showMain();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
